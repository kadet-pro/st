package com.vvv.st.adapters;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vvv.st.databinding.ItemCategoryFoodDetailsBinding;
import com.vvv.st.models.Food;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vovnov on 18.11.2017.
 */

public class CategoryFoodDetailsAdapter extends RecyclerView.Adapter<CategoryFoodDetailsAdapter.ItemViewHolder> {
    private List<Food> list;
    private OnAdapterListener listener;

    public CategoryFoodDetailsAdapter(OnAdapterListener listener) {
        list = new ArrayList<>();
        this.listener = listener;
    }

    public void addAll(List<Food> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(Food item) {
        list.add(item);
        notifyDataSetChanged();
    }

    public void remove(Food task) {
        list.remove(task);
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public CategoryFoodDetailsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryFoodDetailsAdapter.ItemViewHolder(ItemCategoryFoodDetailsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false).getRoot());
    }

    @Override
    public void onBindViewHolder(CategoryFoodDetailsAdapter.ItemViewHolder holder, int position) {
        Food food = list.get(position);
        DecimalFormat precision = new DecimalFormat("0.00");
        holder.binding.name.setText(food.getName());
        double sum = food.getProteins() + food.getFats() + food.getCarbohydrates();
        double percentProteins = 0;
        double percentFats = 0;
        double percentCarbohydrates = 0;
        if (sum > 0) {
            if (food.getProteins() > 0)
                percentProteins = food.getProteins() / sum * 100;
            if (food.getFats() > 0)
                percentFats = food.getFats() / sum * 100;
            if (food.getCarbohydrates() > 0)
                percentCarbohydrates = food.getCarbohydrates() / sum * 100;
        }

        holder.binding.proteins.setProgressWithAnimation((int) percentProteins);
        holder.binding.proteinsPercent.setText(precision.format(percentProteins) + "%");
        holder.binding.proteinsPercent.setTextColor(percentProteins > 0 ? Color.parseColor("#E91E63") : Color.parseColor("#E0E0E0"));

        holder.binding.fat.setProgressWithAnimation((int) percentFats);
        holder.binding.fatPercent.setText(precision.format(percentFats) + "%");
        holder.binding.fatPercent.setTextColor(percentFats > 0 ? Color.parseColor("#8BC34A") : Color.parseColor("#E0E0E0"));

        holder.binding.carbohydrates.setProgressWithAnimation((int) percentCarbohydrates);
        holder.binding.carbohydratesPercent.setText(precision.format(percentCarbohydrates) + "%");
        holder.binding.carbohydratesPercent.setTextColor(percentCarbohydrates > 0 ? Color.parseColor("#FF9800") : Color.parseColor("#E0E0E0"));

        holder.binding.main.setOnClickListener(v -> {
            if (listener != null)
                listener.onClick(food);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private ItemCategoryFoodDetailsBinding binding;

        ItemViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public interface OnAdapterListener {
        void onClick(Food food);
    }
}
