package com.vvv.st.adapters;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.vvv.st.databinding.ItemCategoryFoodBinding;
import com.vvv.st.databinding.ItemCategorySportBinding;
import com.vvv.st.models.Category;
import com.vvv.st.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vovnov on 06.11.2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Category> list;
    private int type;
    private OnAdapterListener listener;

    public CategoryAdapter(OnAdapterListener listener, int type) {
        list = new ArrayList<>();
        this.listener = listener;
        this.type = type;
    }

    public void addItem(Category item) {
        list.add(item);
        notifyDataSetChanged();
    }

    public void addAll(@NonNull List<Category> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (type) {
            case AppConstants.SPORT:
                return new SportViewHolder(ItemCategorySportBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false).getRoot());
            default:
                return new FoodViewHolder(ItemCategoryFoodBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false).getRoot());
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Category mainCategory = list.get(position);
        switch (type) {
            case AppConstants.SPORT:
                SportViewHolder sportViewHolder = (SportViewHolder) holder;
                sportViewHolder.bind(mainCategory);
                break;
            case AppConstants.FOOD:
                FoodViewHolder foodViewHolder = (FoodViewHolder) holder;
                foodViewHolder.bind(mainCategory);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class SportViewHolder extends RecyclerView.ViewHolder {
        private ItemCategorySportBinding binding;

        SportViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        private void bind(Category mainCategory) {
            Glide.with(binding.image.getContext())
                    .load(mainCategory.getImage())
                    .into(binding.image);

            binding.name.setText(mainCategory.getName());

            binding.main.setOnClickListener(v -> {
                if (listener != null)
                    listener.onClick(mainCategory);
            });
        }
    }

    class FoodViewHolder extends RecyclerView.ViewHolder {
        private ItemCategoryFoodBinding binding;

        FoodViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        private void bind(Category mainCategory) {
            Glide.with(binding.image.getContext())
                    .load(mainCategory.getImage())
                    .into(binding.image);

            binding.name.setText(mainCategory.getName());

            binding.main.setOnClickListener(v -> {
                if (listener != null)
                    listener.onClick(mainCategory);
            });
        }
    }

    public interface OnAdapterListener {
        void onClick(Category mainCategory);
    }
}
