package com.vvv.st.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.vvv.st.databinding.ItemCategorySportDetailsBinding;
import com.vvv.st.models.Sport;

import java.util.ArrayList;
import java.util.List;

public class CategorySportDetailsAdapter extends RecyclerView.Adapter<CategorySportDetailsAdapter.ItemViewHolder> {
    private List<Sport> list;
    private OnAdapterListener listener;

    public CategorySportDetailsAdapter(OnAdapterListener listener) {
        list = new ArrayList<>();
        this.listener = listener;
    }

    public void addItem(Sport sport) {
        list.add(sport);
        notifyDataSetChanged();
    }

    public void addAll(List<Sport> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public CategorySportDetailsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategorySportDetailsAdapter.ItemViewHolder(ItemCategorySportDetailsBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false).getRoot());
    }

    @Override
    public void onBindViewHolder(CategorySportDetailsAdapter.ItemViewHolder holder, int position) {
        Sport sport = list.get(position);
        holder.binding.name.setText(sport.getName());
        Glide.with( holder.binding.image.getContext())
                .load(sport.getImages().get(0).getUrl())
                .into( holder.binding.image);
        holder.binding.main.setOnClickListener(v -> {
            if (listener != null)
                listener.onClick(sport);
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private ItemCategorySportDetailsBinding binding;

        ItemViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public interface OnAdapterListener {
        void onClick(Sport sport);
    }
}
