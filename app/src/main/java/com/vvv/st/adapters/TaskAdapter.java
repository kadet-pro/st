package com.vvv.st.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vvv.st.R;
import com.vvv.st.databinding.ItemTaskBinding;
import com.vvv.st.models.Task;
import com.vvv.st.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ItemViewHolder> {
    private List<Task> list;
    private OnAdapterListener listener;

    public TaskAdapter(OnAdapterListener listener) {
        list = new ArrayList<>();
        this.listener = listener;
    }

    public void addAll(List<Task> list) {
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(Task item) {
        list.add(item);
        notifyDataSetChanged();
    }

    public void remove(Task task) {
        list.remove(task);
        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
        notifyDataSetChanged();
    }

    @Override
    public TaskAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TaskAdapter.ItemViewHolder(ItemTaskBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false).getRoot());
    }

    @Override
    public void onBindViewHolder(TaskAdapter.ItemViewHolder holder, int position) {
        Task task = list.get(position);

        holder.binding.delete.setOnClickListener(v -> {
            if (listener != null)
                listener.onClickDelete(task);

        });

        holder.binding.main.setOnClickListener(v -> {
            if (listener != null)
                listener.onClick(task);
        });

        if (!TextUtils.isEmpty(task.getDesc())) {
            holder.binding.desc.setText(task.getDesc());
            holder.binding.desc.setVisibility(View.VISIBLE);
        } else
            holder.binding.desc.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(task.getName())) {
            holder.binding.name.setText(task.getName());
            holder.binding.name.setVisibility(View.VISIBLE);
        } else
            holder.binding.name.setVisibility(View.GONE);

        holder.binding.time.setText(task.getHour() + ":" + task.getMinute());

        holder.binding.typeAlarm.setImageResource(task.getTypeAlarm() == AppConstants.TYPE_NOTIFICATION ? R.drawable.ico_bell : R.drawable.ico_alarm);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private ItemTaskBinding binding;

        ItemViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

    public interface OnAdapterListener {
        void onClick(Task task);

        void onClickDelete(Task task);
    }
}
