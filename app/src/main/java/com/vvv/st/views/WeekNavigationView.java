package com.vvv.st.views;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.vvv.st.R;
import com.vvv.st.databinding.ViewWeekNavigationBinding;

import java.util.Calendar;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class WeekNavigationView extends FrameLayout {
    private static final int BACK_DEFAULT_DAY = R.drawable.back_default_day;
    private static final int BACK_SELECT_DAY = R.drawable.back_select_day;
    private static final int BACK_TODAY = R.drawable.back_today;

    private ViewWeekNavigationBinding binding;
    private OnViewListener listener;
    private int currentDayOfWeek = 0;

    public WeekNavigationView(@NonNull Context context) {
        super(context);
        init();
    }

    public WeekNavigationView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WeekNavigationView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = ViewWeekNavigationBinding.inflate(inflater, this, true);
        initCurrentDay();
        initView();
    }

    private void initView() {
        binding.monday.setOnClickListener(v -> {
            setMonday(BACK_SELECT_DAY);
            setTuesday(BACK_DEFAULT_DAY);
            setWednesday(BACK_DEFAULT_DAY);
            setThursday(BACK_DEFAULT_DAY);
            setFriday(BACK_DEFAULT_DAY);
            setSaturday(BACK_DEFAULT_DAY);
            setSunday(BACK_DEFAULT_DAY);
            if (currentDayOfWeek != Calendar.MONDAY)
                initViewCurrentDay();
            setFocusCenter(binding.monday);
            if (listener != null)
                listener.pickDayOfWeek(Calendar.MONDAY);
        });

        binding.tuesday.setOnClickListener(v -> {
            setMonday(BACK_DEFAULT_DAY);
            setTuesday(BACK_SELECT_DAY);
            setWednesday(BACK_DEFAULT_DAY);
            setThursday(BACK_DEFAULT_DAY);
            setFriday(BACK_DEFAULT_DAY);
            setSaturday(BACK_DEFAULT_DAY);
            setSunday(BACK_DEFAULT_DAY);
            if (currentDayOfWeek != Calendar.TUESDAY)
                initViewCurrentDay();
            setFocusCenter(binding.tuesday);
            if (listener != null)
                listener.pickDayOfWeek(Calendar.TUESDAY);
        });

        binding.wednesday.setOnClickListener(v -> {
            setMonday(BACK_DEFAULT_DAY);
            setTuesday(BACK_DEFAULT_DAY);
            setWednesday(BACK_SELECT_DAY);
            setThursday(BACK_DEFAULT_DAY);
            setFriday(BACK_DEFAULT_DAY);
            setSaturday(BACK_DEFAULT_DAY);
            setSunday(BACK_DEFAULT_DAY);
            if (currentDayOfWeek != Calendar.WEDNESDAY)
                initViewCurrentDay();
            setFocusCenter(binding.wednesday);
            if (listener != null)
                listener.pickDayOfWeek(Calendar.WEDNESDAY);
        });

        binding.thursday.setOnClickListener(v -> {
            setMonday(BACK_DEFAULT_DAY);
            setTuesday(BACK_DEFAULT_DAY);
            setWednesday(BACK_DEFAULT_DAY);
            setThursday(BACK_SELECT_DAY);
            setFriday(BACK_DEFAULT_DAY);
            setSaturday(BACK_DEFAULT_DAY);
            setSunday(BACK_DEFAULT_DAY);
            if (currentDayOfWeek != Calendar.THURSDAY)
                initViewCurrentDay();
            setFocusCenter(binding.thursday);
            if (listener != null)
                listener.pickDayOfWeek(Calendar.THURSDAY);
        });

        binding.friday.setOnClickListener(v -> {
            setMonday(BACK_DEFAULT_DAY);
            setTuesday(BACK_DEFAULT_DAY);
            setWednesday(BACK_DEFAULT_DAY);
            setThursday(BACK_DEFAULT_DAY);
            setFriday(BACK_SELECT_DAY);
            setSaturday(BACK_DEFAULT_DAY);
            setSunday(BACK_DEFAULT_DAY);
            if (currentDayOfWeek != Calendar.FRIDAY)
                initViewCurrentDay();
            setFocusCenter(binding.friday);
            if (listener != null)
                listener.pickDayOfWeek(Calendar.FRIDAY);
        });

        binding.saturday.setOnClickListener(v -> {
            setMonday(BACK_DEFAULT_DAY);
            setTuesday(BACK_DEFAULT_DAY);
            setWednesday(BACK_DEFAULT_DAY);
            setThursday(BACK_DEFAULT_DAY);
            setFriday(BACK_DEFAULT_DAY);
            setSaturday(BACK_SELECT_DAY);
            setSunday(BACK_DEFAULT_DAY);
            if (currentDayOfWeek != Calendar.SATURDAY)
                initViewCurrentDay();
            setFocusCenter(binding.saturday);
            if (listener != null)
                listener.pickDayOfWeek(Calendar.SATURDAY);
        });

        binding.sunday.setOnClickListener(v -> {
            setMonday(BACK_DEFAULT_DAY);
            setTuesday(BACK_DEFAULT_DAY);
            setWednesday(BACK_DEFAULT_DAY);
            setThursday(BACK_DEFAULT_DAY);
            setFriday(BACK_DEFAULT_DAY);
            setSaturday(BACK_DEFAULT_DAY);
            setSunday(BACK_SELECT_DAY);
            if (currentDayOfWeek != Calendar.SUNDAY)
                initViewCurrentDay();
            setFocusCenter(binding.sunday);
            if (listener != null)
                listener.pickDayOfWeek(Calendar.SUNDAY);
        });
    }

    private void initCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        currentDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
    }

    private void initViewCurrentDay() {
        initCurrentDay();
        switch (currentDayOfWeek) {
            case Calendar.MONDAY:
                setMonday(BACK_TODAY);
                break;
            case Calendar.TUESDAY:
                setTuesday(BACK_TODAY);
                break;
            case Calendar.WEDNESDAY:
                setWednesday(BACK_TODAY);
                break;
            case Calendar.THURSDAY:
                setThursday(BACK_TODAY);
                break;
            case Calendar.FRIDAY:
                setFriday(BACK_TODAY);
                break;
            case Calendar.SATURDAY:
                setSaturday(BACK_TODAY);
                break;
            case Calendar.SUNDAY:
                setSunday(BACK_TODAY);
                break;
        }
    }

    private void setMonday(int back) {
        binding.monday.setBackgroundResource(back);
    }

    private void setTuesday(int back) {
        binding.tuesday.setBackgroundResource(back);
    }

    private void setWednesday(int back) {
        binding.wednesday.setBackgroundResource(back);
    }

    private void setThursday(int back) {
        binding.thursday.setBackgroundResource(back);
    }

    private void setFriday(int back) {
        binding.friday.setBackgroundResource(back);
    }

    private void setSaturday(int back) {
        binding.saturday.setBackgroundResource(back);
    }

    private void setSunday(int back) {
        binding.sunday.setBackgroundResource(back);
    }

    public void setListener(OnViewListener listener) {
        this.listener = listener;
        performClickForFirst();
    }

    private void performClickForFirst() {
        initCurrentDay();
        switch (currentDayOfWeek) {
            case Calendar.MONDAY:
                binding.monday.performClick();
                setFocusCenter(binding.monday);
                break;
            case Calendar.TUESDAY:
                binding.tuesday.performClick();
                setFocusCenter(binding.tuesday);
                break;
            case Calendar.WEDNESDAY:
                binding.wednesday.performClick();
                setFocusCenter(binding.wednesday);
                break;
            case Calendar.THURSDAY:
                binding.thursday.performClick();
                setFocusCenter(binding.thursday);
                break;
            case Calendar.FRIDAY:
                binding.friday.performClick();
                setFocusCenter(binding.friday);
                break;
            case Calendar.SATURDAY:
                binding.saturday.performClick();
                setFocusCenter(binding.saturday);
                break;
            case Calendar.SUNDAY:
                binding.sunday.performClick();
                setFocusCenter(binding.sunday);
                break;
        }
    }

    private void setFocusCenter(TextView textView) {
        textView.requestFocus();
        new Handler().post(() -> {
            int vLeft = textView.getLeft();
            int vRight = textView.getRight();
            int sWidth = binding.hsv.getWidth();
            binding.hsv.smoothScrollTo(((vLeft + vRight - sWidth) / 2), 0);
        });
    }

    public interface OnViewListener {
        void pickDayOfWeek(int dayOfWeek);
    }
}
