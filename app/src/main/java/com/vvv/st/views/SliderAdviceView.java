package com.vvv.st.views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.vvv.st.R;
import com.vvv.st.databinding.ViewAdviceBinding;
import com.vvv.st.databinding.ViewSliderAdviceBinding;
import com.vvv.st.models.Sport;

import java.util.ArrayList;
import java.util.List;

public class SliderAdviceView extends FrameLayout {
    private List<Sport.Advice> advices = new ArrayList<>();
    private ViewSliderAdviceBinding binding;

    public SliderAdviceView(Context context) {
        super(context);
        init();
    }

    public SliderAdviceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SliderAdviceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = ViewSliderAdviceBinding.inflate(inflater, this, true);
    }

    public void setAdvices(List<Sport.Advice> advices) {
        this.advices = advices;
        binding.viewPager.setAdapter(new AdapterSlide());
    }

    private class AdapterSlide extends PagerAdapter {
        private ViewAdviceBinding binding;

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_advice, container, false);
            binding.desc.setText(advices.get(position).getDesc());
            container.addView(binding.getRoot());
            return binding.getRoot();
        }

        @Override
        public int getCount() {
            return advices.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}
