package com.vvv.st.views;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.vvv.st.databinding.ViewStepSportBinding;
import com.vvv.st.models.Sport;


public class StepSportView extends FrameLayout {
    private ViewStepSportBinding binding;

    public StepSportView(@NonNull Context context) {
        super(context);
        init();
    }

    public StepSportView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StepSportView(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = ViewStepSportBinding.inflate(inflater, this, true);
    }

    public void setStep(Sport.Step step) {
        binding.num.setText(String.valueOf(step.getId()));
        binding.desc.setText(step.getDesc());
    }
}
