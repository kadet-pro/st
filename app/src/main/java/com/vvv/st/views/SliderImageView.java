package com.vvv.st.views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.vvv.st.R;
import com.vvv.st.databinding.ViewImageBinding;
import com.vvv.st.databinding.ViewSliderBinding;
import com.vvv.st.models.Sport;

import java.util.ArrayList;
import java.util.List;


public class SliderImageView extends FrameLayout {
    private List<Sport.Image> image = new ArrayList<>();
    private ViewSliderBinding binding;
    private boolean isAutoScroll = false;
    private AdapterSlide adapter;
    //auto scroll
    private Handler handler;
    private int page = 0;
    private static final int delay = 2500;
    private TextToSpeech t1;
    private Runnable runnable = new Runnable() {
        public void run() {
            if (adapter.getCount() == page + 1) {
                page = 0;
            } else
                page++;
            binding.viewPager.setCurrentItem(page, true);
            speak();
            handler.postDelayed(this, delay);
        }
    };

    public SliderImageView(Context context) {
        super(context);
        init();
    }

    public SliderImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SliderImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        binding = ViewSliderBinding.inflate(inflater, this, true);
        handler = new Handler();
    }

    public void setImages(List<Sport.Image> image) {
        this.image = image;
        adapter = new AdapterSlide();
        binding.viewPager.setAdapter(adapter);
        binding.indicator.setViewPager(binding.viewPager);
    }

    public void startScroll() {
        page = 0;
        binding.viewPager.setCurrentItem(page, true);
        isAutoScroll = true;
        speak();
        handler.postDelayed(runnable, delay);
    }

    public void removeScroll() {
        if (handler != null)
            handler.removeCallbacks(runnable);
        isAutoScroll = false;
        stopTextToSpeech();
    }

    public boolean isAutoScroll() {
        return isAutoScroll;
    }

    private void stopTextToSpeech() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
    }

    public void setT1(TextToSpeech t1) {
        this.t1 = t1;
    }

    private void speak() {
        if (t1 != null)
            t1.speak(getTextToSpeak(page), TextToSpeech.QUEUE_FLUSH, null);
    }

    private String getTextToSpeak(int times) {
        switch (times) {
            case 0:
                return "Раз";
            case 1:
                return "Два";
            case 2:
                return "Три";
            case 3:
                return "Четыре";
            case 4:
                return "Пять";
            default:
                return "Шесть";
        }
    }

    private class AdapterSlide extends PagerAdapter {
        private ViewImageBinding binding;

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.view_image, container, false);
            Glide.with(getContext())
                    .load(image.get(position).getUrl())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.imageSlider);
            container.addView(binding.getRoot());
            return binding.getRoot();
        }

        @Override
        public int getCount() {
            return image.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}

