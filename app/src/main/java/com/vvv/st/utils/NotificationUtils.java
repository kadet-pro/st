package com.vvv.st.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.vvv.st.R;
import com.vvv.st.activitys.DetailsTaskActivity;

/**
 * Created by Vovnov on 11.11.2017.
 */

public final class NotificationUtils {
    public static void notify(final Context context, String title, String message) {
        notify(context, title, message, new Intent(context, DetailsTaskActivity.class));
    }

    public static void notify(final Context context, String title, String message, Intent intent) {
        if (title == null || title.trim().length() == 0)
            title = context.getString(R.string.app_name);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.logo90)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(
                        PendingIntent.getActivity(
                                context,
                                0,
                                intent,
                                PendingIntent.FLAG_UPDATE_CURRENT))
                .setAutoCancel(true);
        android.app.NotificationManager nm = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(0, builder.build());
    }
}
