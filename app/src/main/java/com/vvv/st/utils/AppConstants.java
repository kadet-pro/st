package com.vvv.st.utils;

/**
 * Created by Vovnov on 06.11.2017.
 */
public final class AppConstants {
    public static final int SPORT = 1;
    public static final int FOOD = 2;

    public static final int TYPE_NOTIFICATION = 101;
    public static final int TYPE_ALARM = 102;
}
