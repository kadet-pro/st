package com.vvv.st.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.vvv.st.R;

/**
 * Created by Vovnov on 06.11.2017.
 */

public final class DialogBuilder {
    public static void showDialogMessage(Context context, String title, String message) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.MyProgressBar);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    public static void showDialogMessage(Context context, String title, String message, AlertDialog.OnClickListener listener) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.MyProgressBar);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    if (listener != null) {
                        listener.onClick(dialog, which);
                    }
                    dialog.dismiss();
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }

    public static void showListDialog(Context context, String title, String[] list, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.MyProgressBar);
        builder.setTitle(title)
                .setCancelable(true)
                .setItems(list, listener);
        builder.create();
        builder.show();
    }

}
