package com.vvv.st.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;



/**
 * Created by Vovnov on 06.11.2017.
 */

public final class PermissionUtils {
    public static boolean checkPermissionFromContext(Context context, String... permissions) {
        try {
            for (String p : permissions) {
                if (ContextCompat.checkSelfPermission(context, p) != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
            return true;
        } catch (NullPointerException ex) {
            Log.e("err", "checkPermissionFromContext: " + ex.getMessage());
            return false;
        }
    }

    public static void requestPermission(Activity activity, String[] permission, int requestCode) {
        ActivityCompat.requestPermissions(activity,
                permission,
                requestCode);
    }

    public static boolean checkPermissionFromActivity(AppCompatActivity activity, String... permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (String p : permission) {
                if (activity.checkSelfPermission(p) != PackageManager.PERMISSION_GRANTED)
                    return false;
            }
        }
        return true;
    }
}
