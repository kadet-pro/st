package com.vvv.st.utils;

import android.content.Context;

import com.orhanobut.hawk.Hawk;

/**
 * Created by Vovnov on 06.11.2017.
 */
public final class AppPreferences {
    public AppPreferences(Context c) {
        Hawk.init(c).build();
    }
}
