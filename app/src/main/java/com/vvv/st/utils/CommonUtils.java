package com.vvv.st.utils;

import com.vvv.st.R;

/**
 * Created by Vovnov on 18.11.2017.
 */

public final class CommonUtils {
    public static int getFoodResourceByCategoryId(int id) {
        switch (id) {
            case 15:
                return R.raw.food_meat;
            case 16:
                return R.raw.food_fish;
            case 17:
                return R.raw.food_eggs;
            case 18:
                return R.raw.food_milk;
            case 19:
                return R.raw.food_fruit;
            case 20:
                return R.raw.food_vegetables;
            case 21:
                return R.raw.food_mushrooms;
            case 22:
                return R.raw.food_herbs;
            case 23:
                return R.raw.food_juices;
            case 24:
                return R.raw.food_cereals;
            case 25:
                return R.raw.food_condiments;
            case 26:
                return R.raw.food_oil;
            case 27:
                return R.raw.food_nuts;
            case 28:
                return R.raw.food_bread;
            default:
                return R.raw.food_other;
        }
    }

    public static int getSportResourceByCategoryId(int id) {
        switch (id) {
            case 1:
                return R.raw.sport_biceps;
            case 2:
                return R.raw.sport_neck;
            case 3:
                return R.raw.sport_chest;
            case 4:
                return R.raw.sport_shoulders;
            case 5:
                return R.raw.sport_forearms;
            case 6:
                return R.raw.sport_press;
            case 7:
                return R.raw.sport_quadriceps;
            case 8:
                return R.raw.sport_trapeze;
            case 9:
                return R.raw.sport_triceps;
            case 10:
                return R.raw.sport_widest_back_muscles;
            case 11:
                return R.raw.sport_lower_back;
            case 12:
                return R.raw.sport_buttocks;
            case 13:
                return R.raw.sport_thighs;
            default:
                return R.raw.sport_calves;
        }
    }
}
