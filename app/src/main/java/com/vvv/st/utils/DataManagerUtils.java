package com.vvv.st.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vvv.st.models.Category;
import com.vvv.st.models.Food;
import com.vvv.st.models.Sport;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;


/**
 * Created by Vovnov on 06.11.2017.
 */

public final class DataManagerUtils {
    private static String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            return new String(bytes);
        } catch (IOException e) {
            return null;
        }
    }

    public static Flowable<List<Category>> getCategorys(InputStream inputStream) {
        return Flowable.create(flowableEmitter -> {
            String JsonMainCategory = inputStreamToString(inputStream);
            if (JsonMainCategory != null) {
                Type listType = new TypeToken<ArrayList<Category>>() {
                }.getType();
                List<Category> list = new Gson().fromJson(JsonMainCategory, listType);
                if (list != null) {
                    flowableEmitter.onNext(list);
                } else flowableEmitter.onError(new RuntimeException("list NullPointerException"));
            } else
                flowableEmitter.onError(new RuntimeException("JsonMainCategory NullPointerException"));
        }, BackpressureStrategy.BUFFER);
    }

    public static Flowable<List<Food>> getFoods(InputStream inputStream) {
        return Flowable.create(flowableEmitter -> {
            String JsonMainFoods = inputStreamToString(inputStream);
            if (JsonMainFoods != null) {
                Type listType = new TypeToken<ArrayList<Food>>() {
                }.getType();
                List<Food> list = new Gson().fromJson(JsonMainFoods, listType);
                if (list != null) {
                    flowableEmitter.onNext(list);
                } else flowableEmitter.onError(new RuntimeException("list NullPointerException"));
            } else
                flowableEmitter.onError(new RuntimeException("JsonMainFoods NullPointerException"));
        }, BackpressureStrategy.BUFFER);
    }

    public static Flowable<List<Sport>> getSports(InputStream inputStream) {
        return Flowable.create(flowableEmitter -> {
            String JsonMainSports = inputStreamToString(inputStream);
            if (JsonMainSports != null) {
                Type listType = new TypeToken<ArrayList<Sport>>() {
                }.getType();
                List<Sport> list = new Gson().fromJson(JsonMainSports, listType);
                if (list != null) {
                    flowableEmitter.onNext(list);
                } else flowableEmitter.onError(new RuntimeException("list NullPointerException"));
            } else
                flowableEmitter.onError(new RuntimeException("JsonMainSports NullPointerException"));
        }, BackpressureStrategy.BUFFER);
    }
}
