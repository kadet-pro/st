package com.vvv.st.database;

import android.util.Log;

import com.vvv.st.models.Task;

import java.util.List;


import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.Sort;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class DBUtils {

    private static Realm getRealm() {
        return Realm.getDefaultInstance();
    }

    private static int getAutoIncrementTask() {
        try {
            Realm realm = getRealm();
            return realm.where(Task.class).max("id").intValue() + 1;
        } catch (Exception ex) {
            Log.e("TAB_LOG", "getAutoIncrement: " + ex);
            return 1;
        }
    }

    public static int addTask(Task task) {
        int id;
        Realm realm = getRealm();
        Task check = realm.where(Task.class).equalTo("id", task.getId()).findFirst();
        realm.beginTransaction();
        if (check == null) {
            id = getAutoIncrementTask();
            Task newTask = realm.createObject(Task.class);
            newTask.setId(id);
            newTask.setDayOfWeek(task.getDayOfWeek());
            newTask.setDesc(task.getDesc());
            newTask.setHour(task.getHour());
            newTask.setMinute(task.getMinute());
            newTask.setTypeAlarm(task.getTypeAlarm());
            newTask.setName(task.getName());
        } else {
            id = check.getId();
            check.setDayOfWeek(task.getDayOfWeek());
            check.setDesc(task.getDesc());
            check.setHour(task.getHour());
            check.setMinute(task.getMinute());
            check.setTypeAlarm(task.getTypeAlarm());
            check.setName(task.getName());
        }
        realm.commitTransaction();
        realm.close();
        return id;
    }

    public static void deleteTask(Task task) {
        Realm realm = getRealm();
        Task check = realm.where(Task.class).equalTo("id", task.getId()).findFirst();
        if (check != null) {
            realm.beginTransaction();
            check.deleteFromRealm();
            realm.commitTransaction();
        }
        realm.close();
    }

    public static Observable<List<Task>> getTasks() {
        return Observable.just(getRealm().copyFromRealm(getRealm().where(Task.class).findAll()));
    }

    public static Observable<List<Task>> getTasks(int dayOfWeek) {
        return Observable.just(getRealm().copyFromRealm(getRealm().where(Task.class).equalTo("dayOfWeek", dayOfWeek).findAllSorted("hour", Sort.ASCENDING, "minute", Sort.ASCENDING)));
    }

    public static Task getTask(int id) {
        return getRealm().copyFromRealm(getRealm().where(Task.class).equalTo("id", id).findFirst());
    }
}
