package com.vvv.st.models;

import io.realm.RealmObject;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class Task extends RealmObject {
    private int id;
    private int dayOfWeek;
    private String desc;
    private int hour;
    private int minute;
    private int typeAlarm;
    private String name;

    public Task() {
    }

    public Task(int dayOfWeek, String desc, int hour, int minute, int typeAlarm, String name) {
        this.dayOfWeek = dayOfWeek;
        this.desc = desc;
        this.hour = hour;
        this.minute = minute;
        this.typeAlarm = typeAlarm;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getTypeAlarm() {
        return typeAlarm;
    }

    public void setTypeAlarm(int typeAlarm) {
        this.typeAlarm = typeAlarm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != task.id) return false;
        if (dayOfWeek != task.dayOfWeek) return false;
        if (hour != task.hour) return false;
        if (minute != task.minute) return false;
        if (typeAlarm != task.typeAlarm) return false;
        if (desc != null ? !desc.equals(task.desc) : task.desc != null) return false;
        return name != null ? name.equals(task.name) : task.name == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + dayOfWeek;
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + hour;
        result = 31 * result + minute;
        result = 31 * result + typeAlarm;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
