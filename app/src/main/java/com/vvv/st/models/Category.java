package com.vvv.st.models;

import java.io.Serializable;

/**
 * Created by Vovnov on 06.11.2017.
 */

public class Category implements Serializable {
    private String name;
    private int id;
    private int type;
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (id != category.id) return false;
        if (type != category.type) return false;
        if (name != null ? !name.equals(category.name) : category.name != null) return false;
        return image != null ? image.equals(category.image) : category.image == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + id;
        result = 31 * result + type;
        result = 31 * result + (image != null ? image.hashCode() : 0);
        return result;
    }
}
