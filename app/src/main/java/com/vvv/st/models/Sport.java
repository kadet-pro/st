package com.vvv.st.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vovnov on 19.11.2017.
 */

public class Sport implements Serializable {
    private int id;
    @SerializedName("id_category")
    private int idCategory;
    private String name;
    private String desc;
    private List<Image> images;
    private List<Step> steps;
    private List<Advice> advices;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public List<Advice> getAdvices() {
        return advices;
    }

    public void setAdvices(List<Advice> advices) {
        this.advices = advices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sport sport = (Sport) o;

        if (id != sport.id) return false;
        if (idCategory != sport.idCategory) return false;
        if (name != null ? !name.equals(sport.name) : sport.name != null) return false;
        if (desc != null ? !desc.equals(sport.desc) : sport.desc != null) return false;
        if (images != null ? !images.equals(sport.images) : sport.images != null) return false;
        if (steps != null ? !steps.equals(sport.steps) : sport.steps != null) return false;
        return advices != null ? advices.equals(sport.advices) : sport.advices == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + idCategory;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (desc != null ? desc.hashCode() : 0);
        result = 31 * result + (images != null ? images.hashCode() : 0);
        result = 31 * result + (steps != null ? steps.hashCode() : 0);
        result = 31 * result + (advices != null ? advices.hashCode() : 0);
        return result;
    }

    public static class Advice implements Serializable {
        private int id;
        private String desc;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    public static class Step implements Serializable {
        private int id;
        private String desc;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    public static class Image implements Serializable {
        private int id;
        private String url;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
