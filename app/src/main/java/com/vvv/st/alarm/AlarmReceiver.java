package com.vvv.st.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vvv.st.R;
import com.vvv.st.activitys.AlarmActivity;
import com.vvv.st.activitys.DetailsTaskActivity;
import com.vvv.st.database.DBUtils;
import com.vvv.st.models.Task;
import com.vvv.st.utils.AppConstants;
import com.vvv.st.utils.NotificationUtils;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            int id = intent.getIntExtra("id", 0);
            if (id != 0) {
                Task task = DBUtils.getTask(id);
                if (task != null) {
                    if (task.getTypeAlarm() == AppConstants.TYPE_NOTIFICATION) {
                        Intent intentOpen = new Intent(context, DetailsTaskActivity.class);
                        intentOpen.putExtra("id", id);
                        intentOpen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        NotificationUtils.notify(context, context.getString(R.string.app_name), task.getName(), intentOpen);
                    } else {
                        Intent intentOpen = new Intent(context, AlarmActivity.class);
                        intentOpen.putExtra("id", id);
                        intentOpen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intentOpen);
                    }
                }
            }
        }
    }
}
