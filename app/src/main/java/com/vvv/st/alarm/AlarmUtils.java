package com.vvv.st.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.vvv.st.models.Task;

import java.util.Calendar;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class AlarmUtils {
    public static void add(Context context, Task task) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        alarmIntent.putExtra("id", task.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), task.getId(), alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.DAY_OF_WEEK, task.getDayOfWeek());
        calendar.set(Calendar.HOUR_OF_DAY, task.getHour());
        calendar.set(Calendar.MINUTE, task.getMinute());
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        if (calendar.getTimeInMillis() <= System.currentTimeMillis())
            calendar.setTimeInMillis(calendar.getTimeInMillis() + 7 * 24 * 60 * 60 * 1000);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 7 * (24 * 60 * 60 * 1000), pendingIntent);
    }

    public static void cancel(Context context, Task task) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, task.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }
}
