package com.vvv.st.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vvv.st.database.DBUtils;
import com.vvv.st.models.Task;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class DeviceBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            DBUtils.getTasks()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(tasks -> {
                        for (Task task : tasks)
                            AlarmUtils.add(context, task);
                    });
        }
    }
}
