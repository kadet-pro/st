package com.vvv.st;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.vvv.st.injection.AppComponent;
import com.vvv.st.injection.ApplicationModule;
import com.vvv.st.injection.DaggerAppComponent;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;


/**
 * Created by Vovnov on 06.11.2017.
 */

public class StApp extends Application {
    public static Context applicationContext;
    public static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        applicationContext = getApplicationContext();
        component = DaggerAppComponent.builder().applicationModule(new ApplicationModule(this)).build();
        //realm
        Realm.init(this);

        RealmConfiguration.Builder builder = new RealmConfiguration.Builder()
                .name("com.vvv.st")
                .schemaVersion(1);

        if (BuildConfig.DEBUG)
            builder.deleteRealmIfMigrationNeeded();

        Realm.setDefaultConfiguration(builder.build());
    }

    public static Context getContext() {
        return applicationContext;
    }
}
