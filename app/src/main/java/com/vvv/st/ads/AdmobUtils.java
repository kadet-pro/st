package com.vvv.st.ads;

import android.util.Log;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by Vovnov on 24.11.2017.
 */

public class AdmobUtils {
    public static void showBanner(final AdView banner) {
        AdRequest adRequest = new AdRequest.Builder().build();
        banner.loadAd(adRequest);
        banner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d("tab_log", "onAdLoaded: ");
                banner.setVisibility(View.VISIBLE);
            }
        });
    }
}
