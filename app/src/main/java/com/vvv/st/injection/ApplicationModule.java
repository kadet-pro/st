package com.vvv.st.injection;

import android.app.Application;
import android.content.Context;

import com.vvv.st.utils.AppPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Vovnov on 06.11.2017.
 */

@Module
public class ApplicationModule {
    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    public AppPreferences provideAppPreferences(Context context) {
        return new AppPreferences(context);
    }
}
