package com.vvv.st.injection;

import com.vvv.st.activitys.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Vovnov on 06.11.2017.
 */
@Component(modules = ApplicationModule.class)
@Singleton
public interface AppComponent {
    void inject(MainActivity activity);
}
