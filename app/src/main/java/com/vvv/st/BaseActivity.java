package com.vvv.st;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.ads.MobileAds;

import io.reactivex.disposables.Disposable;

/**
 * Created by Vovnov on 06.11.2017.
 */

public class BaseActivity extends AppCompatActivity {
    protected Disposable subscriber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(this, getString(R.string.ad_mob_app_id));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unsubscribe();
    }

    protected void unsubscribe() {
        if (subscriber != null) {
            subscriber.dispose();
        }
    }
}
