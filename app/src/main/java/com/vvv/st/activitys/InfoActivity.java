package com.vvv.st.activitys;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.vvv.st.BaseActivity;
import com.vvv.st.BuildConfig;
import com.vvv.st.R;
import com.vvv.st.databinding.ActivityInfoBinding;
import com.vvv.st.utils.AppUtils;

/**
 * Created by Vovnov on 24.11.2017.
 */

public class InfoActivity extends BaseActivity {
    private ActivityInfoBinding binding;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, InfoActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_info);
        binding.version.setText("Version " + BuildConfig.VERSION_NAME);
    }


    public void onClickWriteUs(View view) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + "vvvteamreal@gmail.com"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        startActivity(Intent.createChooser(emailIntent, getString(R.string.app_name)));
    }

    public void onClickEstimateUs(View view) {
        AppUtils.openPlayStoreForApp(this);
    }


    public void onClickBack(View view) {
        finish();
    }

    public void onClickShareUs(View view) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getString(R.string.share_text));
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
}
