package com.vvv.st.activitys;


import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.StApp;
import com.vvv.st.adapters.TaskAdapter;
import com.vvv.st.ads.AdmobUtils;
import com.vvv.st.alarm.AlarmUtils;
import com.vvv.st.database.DBUtils;
import com.vvv.st.databinding.ActivityMainBinding;
import com.vvv.st.models.Task;
import com.vvv.st.utils.AppConstants;
import com.vvv.st.utils.AppPreferences;
import com.vvv.st.utils.DialogBuilder;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.content.DialogInterface.BUTTON_POSITIVE;


public class MainActivity extends BaseActivity {
    private static final int REQUEST_CODE_ADD_TASK = 10;
    private static final int REQUEST_CODE_DETAILS_TASK = 20;
    private ActivityMainBinding binding;
    private TaskAdapter adapter;
    private int dayOfWeek = 1;

    @Inject
    AppPreferences preferences;


    public static void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        StApp.component.inject(this);
        initRecycler();
        binding.weekNavigation.setListener(this::loadTasks);
        binding.fab.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, AddTaskActivity.class);
            intent.putExtra("day_of_week", dayOfWeek);
            startActivityForResult(intent, REQUEST_CODE_ADD_TASK);
        });
        AdmobUtils.showBanner(binding.banner);
    }

    private void initRecycler() {
        adapter = new TaskAdapter(new TaskAdapter.OnAdapterListener() {
            @Override
            public void onClick(Task task) {
                Intent intent = new Intent(MainActivity.this, DetailsTaskActivity.class);
                intent.putExtra("id", task.getId());
                startActivityForResult(intent, REQUEST_CODE_DETAILS_TASK);
            }

            @Override
            public void onClickDelete(final Task task) {
                DialogBuilder.showDialogMessage(MainActivity.this, getString(R.string.message), getString(R.string.do_you_want_delete_the_task), (dialog, which) -> {
                    if (which == BUTTON_POSITIVE) {
                        AlarmUtils.cancel(MainActivity.this, task);
                        DBUtils.deleteTask(task);
                        adapter.remove(task);
                    }
                });
            }
        });
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recycler.setLayoutManager(llm);
        binding.recycler.setAdapter(adapter);
        binding.recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    if (binding.fab.isShown()) {
                        binding.fab.hide();
                    }
                } else if (dy < 0) {
                    if (!binding.fab.isShown()) {
                        binding.fab.show();
                    }
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_ADD_TASK) {
            loadTasks(dayOfWeek);
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_DETAILS_TASK) {
            loadTasks(dayOfWeek);
        }
    }

    private void loadTasks(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        binding.fab.show();
        adapter.clear();
        subscriber = DBUtils.getTasks(dayOfWeek)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> adapter.addAll(list));
    }

    public void onClickSport(View view) {
        CategoryActivity.startActivity(this, AppConstants.SPORT);
    }

    public void onClickFood(View view) {
        CategoryActivity.startActivity(this, AppConstants.FOOD);
    }

    public void onClickInfo(View view) {
        InfoActivity.startActivity(this);
    }
}

