package com.vvv.st.activitys;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.ads.AdmobUtils;
import com.vvv.st.databinding.ActivitySportDetailsBinding;
import com.vvv.st.models.Sport;
import com.vvv.st.views.StepSportView;

import java.util.Locale;

public class SportDetailsActivity extends BaseActivity {
    private final int MY_DATA_CHECK_CODE = 0;
    private ActivitySportDetailsBinding binding;
    private Sport sport;
    private TextToSpeech myTTS;

    public static void startActivity(Context context, Sport sport) {
        Intent intent = new Intent(context, SportDetailsActivity.class);
        intent.putExtra("sport", sport);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sport_details);
        initParams();
    }

    private void initParams() {
        if (getIntent() != null) {
            sport = (Sport) getIntent().getSerializableExtra("sport");
            if (sport != null) {
                initData();
            } else finish();
        } else finish();
    }

    private void initData() {
        AdmobUtils.showBanner(binding.banner);
        binding.title.setText(sport.getName());
        binding.desc.setText(sport.getDesc());
        if (sport.getImages() != null && sport.getImages().size() <= 1) {
            binding.play.setVisibility(View.GONE);
        }
        binding.sliderImage.setImages(sport.getImages());
        for (Sport.Step step : sport.getSteps()) {
            StepSportView view = new StepSportView(this);
            view.setStep(step);
            binding.steps.addView(view);
        }
        binding.sliderAdvice.setAdvices(sport.getAdvices());

        try {
            Intent checkTTSIntent = new Intent();
            checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
            startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);
        } catch (ActivityNotFoundException ex) {
            binding.play.setVisibility(View.GONE);
        }
    }

    public void onClickBack(View view) {
        finish();
    }

    public void onClickPlay(View view) {
        if (binding.sliderImage.isAutoScroll()) {
            binding.sliderImage.removeScroll();
            binding.play.setImageResource(R.drawable.ico_play);
        } else {
            binding.nestedScroll.scrollTo(0, 0);
            binding.sliderImage.startScroll();
            binding.play.setImageResource(R.drawable.ico_pause);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.sliderImage.removeScroll();
        binding.play.setImageResource(R.drawable.ico_play);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                myTTS = new TextToSpeech(this, status -> {
                    if (status == TextToSpeech.SUCCESS) {
                        if (myTTS.isLanguageAvailable(Locale.US) == TextToSpeech.LANG_AVAILABLE)
                            myTTS.setLanguage(Locale.US);
                        binding.sliderImage.setT1(myTTS);
                    } else if (status == TextToSpeech.ERROR) {
                        Toast.makeText(SportDetailsActivity.this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }

}
