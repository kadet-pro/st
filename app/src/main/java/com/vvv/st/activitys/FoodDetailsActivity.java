package com.vvv.st.activitys;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.ads.AdmobUtils;
import com.vvv.st.databinding.ActivityFoodDetailsBinding;
import com.vvv.st.models.Food;

/**
 * Created by Vovnov on 19.11.2017.
 */

public class FoodDetailsActivity extends BaseActivity {
    private ActivityFoodDetailsBinding binding;
    private Food food;

    public static void startActivity(Context context, Food food) {
        Intent intent = new Intent(context, FoodDetailsActivity.class);
        intent.putExtra("food", food);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_food_details);
        initParams();
    }

    private void initParams() {
        if (getIntent() != null) {
            food = (Food) getIntent().getSerializableExtra("food");
            if (food != null) {
                initData();
            } else finish();
        } else finish();
    }

    private void initData() {
        AdmobUtils.showBanner(binding.banner);
        binding.title.setText(food.getName());
        binding.desc.setText(food.getDesc());
        binding.calories.setText(getString(R.string.calories) + " " + food.getCalories());
        binding.fats.setText(getString(R.string.fat) + " " + food.getFats() + " г");
        binding.proteins.setText(getString(R.string.proteins) + " " + food.getProteins() + " г");
        binding.carbohydrates.setText(getString(R.string.carbohydrates) + " " + food.getCarbohydrates() + " г");
    }

    public void onClickBack(View view) {
        finish();
    }
}
