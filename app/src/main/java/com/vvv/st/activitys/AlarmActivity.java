package com.vvv.st.activitys;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.WindowManager;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.database.DBUtils;
import com.vvv.st.databinding.ActivityAlarmBinding;
import com.vvv.st.models.Task;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class AlarmActivity extends BaseActivity {
    private ActivityAlarmBinding binding;
    private Ringtone r;
    private Task task;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_alarm);
        setSettingWindow();
        initParams();
        r = RingtoneManager.getRingtone(getApplicationContext(), setSound());
        start();
        binding.stop.setOnClickListener(v -> {
            if (r != null && r.isPlaying())
                r.stop();
            Intent intent = new Intent(AlarmActivity.this, DetailsTaskActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("id", task.getId());
            startActivity(intent);
        });
    }

    private void initParams() {
        if (getIntent() != null) {
            int id = getIntent().getIntExtra("id", 0);
            if (id != 0) {
                Task task = DBUtils.getTask(id);
                if (task != null) {
                    this.task = task;
                    binding.name.setText(task.getName());
                } else finish();
            } else finish();
        } else finish();
    }

    private void start() {
        binding.content.startRippleAnimation();
        r.play();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (r != null && r.isPlaying())
            r.stop();
    }

    private Uri setSound() {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }

    private void setSettingWindow() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

}
