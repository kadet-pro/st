package com.vvv.st.activitys;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.ads.AdmobUtils;
import com.vvv.st.alarm.AlarmUtils;
import com.vvv.st.database.DBUtils;
import com.vvv.st.databinding.ActivityDetailsTaskBinding;
import com.vvv.st.models.Task;
import com.vvv.st.utils.DialogBuilder;

import static android.content.DialogInterface.BUTTON_POSITIVE;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class DetailsTaskActivity extends BaseActivity {
    private ActivityDetailsTaskBinding binding;
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details_task);
        initParams();
        initView();
    }

    private void initParams() {
        if (getIntent() != null) {
            int id = getIntent().getIntExtra("id", 0);
            if (id != 0) {
                Task task = DBUtils.getTask(id);
                if (task != null) {
                    this.task = task;
                } else finish();
            } else finish();
        } else finish();
    }

    private void initView() {
        AdmobUtils.showBanner(binding.banner);
        if (!TextUtils.isEmpty(task.getName()))
            binding.title.setText(task.getName());
        if (!TextUtils.isEmpty(task.getDesc()))
            binding.desc.setText(task.getDesc());
        binding.time.setText(task.getHour() + ":" + task.getMinute());
    }

    public void onClickBack(View view) {
        finish();
    }

    public void onClickDelete(View view) {
        DialogBuilder.showDialogMessage(this, getString(R.string.message), getString(R.string.do_you_want_delete_the_task), (dialog, which) -> {
            if (which == BUTTON_POSITIVE) {
                DBUtils.deleteTask(task);
                AlarmUtils.cancel(this, task);
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}
