package com.vvv.st.activitys;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.databinding.ActivitySplashBinding;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by Vovnov on 06.11.2017.
 */

public class SplashActivity extends BaseActivity {
    private ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        delay();
    }

    private void delay() {
        subscriber = Observable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(i -> MainActivity.startActivity(SplashActivity.this));
    }
}
