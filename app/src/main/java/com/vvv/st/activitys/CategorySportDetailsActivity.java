package com.vvv.st.activitys;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.adapters.CategorySportDetailsAdapter;
import com.vvv.st.ads.AdmobUtils;
import com.vvv.st.databinding.ActivityCategorySportDetailsBinding;
import com.vvv.st.models.Category;
import com.vvv.st.utils.CommonUtils;
import com.vvv.st.utils.DataManagerUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vovnov on 19.11.2017.
 */

public class CategorySportDetailsActivity extends BaseActivity {
    private ActivityCategorySportDetailsBinding binding;
    private Category category;
    private CategorySportDetailsAdapter adapter;

    public static void startActivity(Context context, Category category) {
        Intent intent = new Intent(context, CategorySportDetailsActivity.class);
        intent.putExtra("category", category);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category_sport_details);
        initRecycler();
        initParams();
    }

    private void initRecycler() {
        adapter = new CategorySportDetailsAdapter(sport -> SportDetailsActivity.startActivity(this, sport));
        binding.recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.recycler.setAdapter(adapter);
    }

    private void initParams() {
        if (getIntent() != null) {
            category = (Category) getIntent().getSerializableExtra("category");
            if (category != null) {
                initData();
            } else finish();
        } else finish();
    }

    private void initData() {
        AdmobUtils.showBanner(binding.banner);
        binding.title.setText(category.getName());
        subscriber = DataManagerUtils.getSports(getResources().openRawResource(CommonUtils.getSportResourceByCategoryId(category.getId())))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sports -> {
                    adapter.addAll(sports);
                });
    }

    public void onClickBack(View view) {
        finish();
    }
}
