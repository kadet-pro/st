package com.vvv.st.activitys;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.adapters.CategoryAdapter;
import com.vvv.st.databinding.ActivityCategoryBinding;
import com.vvv.st.utils.AppConstants;
import com.vvv.st.utils.DataManagerUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vovnov on 06.11.2017.
 */

public class CategoryActivity extends BaseActivity {

    private ActivityCategoryBinding binding;
    private CategoryAdapter adapter;
    private int type;

    public static void startActivity(Context context, int type) {
        Intent intent = new Intent(context, CategoryActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category);
        initParams();
        initRecycler();
        initData();
    }

    private void initParams() {
        if (getIntent() != null) {
            type = getIntent().getIntExtra("type", AppConstants.SPORT);
            binding.title.setText(type == AppConstants.SPORT ? getString(R.string.group_muscle) : getString(R.string.food_product));
        }
    }

    private void initRecycler() {
        adapter = new CategoryAdapter(category -> {
            if (type == AppConstants.FOOD)
                CategoryFoodDetailsActivity.startActivity(this, category);
            else
                CategorySportDetailsActivity.startActivity(this, category);
        }, type);
        if (type == AppConstants.SPORT)
            binding.recycler.setLayoutManager(new GridLayoutManager(this, 2));
        else
            binding.recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        binding.recycler.setAdapter(adapter);
    }

    private void initData() {
        subscriber = DataManagerUtils.getCategorys(getResources().openRawResource(R.raw.category))
                .subscribeOn(Schedulers.io())
                .flatMap(Flowable::fromIterable)
                .filter(category -> category.getType() == type)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mainCategories -> adapter.addItem(mainCategories));
    }

    public void onClickBack(View view) {
        finish();
    }
}
