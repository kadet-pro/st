package com.vvv.st.activitys;

import android.app.TimePickerDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.vvv.st.BaseActivity;
import com.vvv.st.R;
import com.vvv.st.ads.AdmobUtils;
import com.vvv.st.alarm.AlarmUtils;
import com.vvv.st.database.DBUtils;
import com.vvv.st.databinding.ActivityAddTaskBinding;
import com.vvv.st.models.Task;
import com.vvv.st.utils.AppConstants;
import com.vvv.st.utils.DialogBuilder;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Vovnov on 11.11.2017.
 */

public class AddTaskActivity extends BaseActivity {
    private static final int STEP_SELECT_DAYS = 1;
    private static final int STEP_INPUT_TIME_TEXT = 2;
    private int hour;
    private int minute;
    private int currentStep = STEP_SELECT_DAYS;
    private Set<Integer> days = new HashSet<>();
    private ActivityAddTaskBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_task);
        AdmobUtils.showBanner(binding.banner);
        initParams();
        initDaysView();
    }

    private void initParams() {
        if (getIntent() != null) {
            int days = getIntent().getIntExtra("day_of_week", 0);
            if (days != 0) {
                this.days.add(days);
                switch (days) {
                    case Calendar.MONDAY:
                        binding.monday.setChecked(true);
                        break;
                    case Calendar.TUESDAY:
                        binding.tuesday.setChecked(true);
                        break;
                    case Calendar.WEDNESDAY:
                        binding.wednesday.setChecked(true);
                        break;
                    case Calendar.THURSDAY:
                        binding.thursday.setChecked(true);
                        break;
                    case Calendar.FRIDAY:
                        binding.friday.setChecked(true);
                        break;
                    case Calendar.SATURDAY:
                        binding.saturday.setChecked(true);
                        break;
                    case Calendar.SUNDAY:
                        binding.sunday.setChecked(true);
                        break;
                }
            }
        }
    }

    private void initDaysView() {
        binding.monday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                days.add(Calendar.MONDAY);
            else
                days.remove(Calendar.MONDAY);
        });

        binding.tuesday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                days.add(Calendar.TUESDAY);
            else
                days.remove(Calendar.TUESDAY);
        });

        binding.wednesday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                days.add(Calendar.WEDNESDAY);
            else
                days.remove(Calendar.WEDNESDAY);
        });

        binding.thursday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                days.add(Calendar.THURSDAY);
            else
                days.remove(Calendar.THURSDAY);
        });

        binding.friday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                days.add(Calendar.FRIDAY);
            else
                days.remove(Calendar.FRIDAY);
        });

        binding.saturday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                days.add(Calendar.SATURDAY);
            else
                days.remove(Calendar.SATURDAY);
        });

        binding.sunday.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked)
                days.add(Calendar.SUNDAY);
            else
                days.remove(Calendar.SUNDAY);
        });
    }

    public void onClickBack(View view) {
        switch (currentStep) {
            case STEP_SELECT_DAYS:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case STEP_INPUT_TIME_TEXT:
                currentStep = STEP_SELECT_DAYS;
                binding.days.setVisibility(View.VISIBLE);
                binding.input.setVisibility(View.GONE);

                binding.backIco.setVisibility(View.GONE);
                binding.backText.setText(getString(R.string.cancel));

                binding.nextIco.setVisibility(View.VISIBLE);
                binding.nextText.setText(getString(R.string.next));
                break;
        }
    }

    public void onClickNext(View view) {
        switch (currentStep) {
            case STEP_SELECT_DAYS:
                if (days.size() > 0) {
                    currentStep = STEP_INPUT_TIME_TEXT;

                    binding.days.setVisibility(View.GONE);
                    binding.input.setVisibility(View.VISIBLE);

                    binding.backIco.setVisibility(View.VISIBLE);
                    binding.backText.setText(R.string.back);

                    binding.nextIco.setVisibility(View.GONE);
                    binding.nextText.setText(R.string.create);
                } else
                    DialogBuilder.showDialogMessage(this, getString(R.string.message), getString(R.string.select_one_day_on_week));
                break;
            case STEP_INPUT_TIME_TEXT:
                if (TextUtils.isEmpty(binding.time.getText().toString())) {
                    DialogBuilder.showDialogMessage(this, getString(R.string.message), getString(R.string.select_time_alarm_task));
                    return;
                }

                if (TextUtils.isEmpty(binding.name.getText().toString())) {
                    DialogBuilder.showDialogMessage(this, getString(R.string.message), getString(R.string.name_task_required_field));
                    return;
                }
                save();
                break;
        }
    }

    private void save() {
        for (Integer integer : days) {
            Task task = new Task(integer, binding.description.getText().toString(), hour, minute, binding.notification.isChecked() ? AppConstants.TYPE_NOTIFICATION : AppConstants.TYPE_ALARM, binding.name.getText().toString());
            int id = DBUtils.addTask(task);
            task.setId(id);
            AlarmUtils.add(this, task);
        }
        setResult(RESULT_OK);
        finish();
    }

    public void onClickTime(View view) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        TimePickerDialog mTimePicker = new TimePickerDialog(this, (timePicker, selectedHour, selectedMinute) -> {
            hour = selectedHour;
            minute = selectedMinute;
            binding.time.setText(selectedHour + ":" + selectedMinute);
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        mTimePicker.setTitle(getString(R.string.select_time));
        mTimePicker.show();
    }

    public void onClickShareUs(View view) {
    }
}
